import os
import torch.nn as nn
import torch.nn.functional as F
import logging
import logging.config
import torch.optim 
import db
import torch
_path = os.path.dirname(os.path.abspath(__file__))     

class ConvNet(nn.Module):
    def __init__(self, inchannel=1):
        '''
        define convolutional layers,  
        followed by two fully connected layers to transform 
        the output of the convolution layers to the final output
        '''
        self.conv1 = nn.Conv2d(in_channels=inchannel, out_channels=32,
            kernel_size=3, strid=1, padding=1)
        self.bn1 = nn.BatchNorm2d(32)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64,
            kernel_size=3, stride=1, padding=1)
        self.bn2 = nn.BatchNorm2d(64)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=128,
            kernel_size=3, stride=1, padding=1)
        self.bn3 = nn.BatchNorm2d(128)
                
        self.fc1 = nn.Linear(in_features=8*8*128, out_features=128)
        self.fcbn1 = nn.BatchNorm1d(128)
        self.fc2 = nn.Linear(in_features=128, out_features=6)       
        self.dropout_rate = hyperparams.dropout_rate

    def forward(self, x):
        '''
        apply the convolution layers, followed by batch normalisation, 
        maxpool and relu x 3
        '''
        step = self.bn1(self.conv1(step))        # batch_size x 32 x 64 x 64
        step = F.relu(F.max_pool2d(step, 2))     # batch_size x 32 x 32 x 32
        step = self.bn2(self.conv2(step))        # batch_size x 64 x 32 x 32
        step = F.relu(F.max_pool2d(step, 2))     # batch_size x 64 x 16 x 16
        step = self.bn3(self.conv3(step))        # batch_size x 128 x 16 x 16
        step = F.relu(F.max_pool2d(step, 2))     # batch_size x 128 x 8 x 8
                
        # flatten the output for each image
        step = step.view(-1, 8*8*128)  # batch_size x 8*8*128
                
        # apply 2 fully connected layers with dropout
        step = F.dropout(F.relu(self.fcbn1(self.fc1(step))),
            p=self.dropout_rate, training=self.training)    # batch_size x 128
        step = self.fc2(step)                               # batch_size x 6
                
        return F.log_softmax(step, dim=1)


class Learner(object):
    '''
    train / evaluate for model using data to minimize loss_func with optimizer 
    '''
    def __init__(self, db_session):
        # network setup
        self.model = ConvNet()
        self.opt_func = opt_func
        self.loss_function = nn.CrossEntropyLoss()
        self.device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.optimizer = optim.SGD(n=model.parameters(), lr=0.001, momentum=0.9)
        # logging
        fileConfig(f'{_path}/logger.ini', disable_existing_loggers=False)
        self.logger = logging.getLogger(self.__class__.__name__)
        # database
        self.db_session = db_session

    def export(self, path='./', fname='trained_state.pkl'):
        '''
        Export the state of the `Learner` in `self.path/fname`.
        '''
        torch.save({
            'model_state_dict': model.state_dict(),
            'optimizer_state_dict': optimizer.state_dict(),
            'loss': loss,
            }, os.path.join(path, fname))
        with open(os.path.join(path, self.previous_device), 'w') as f:
            f.write(self.device)

    def load_checkpoint(self, path='./', fname='trained_state.pkl'):
        '''
        identify on which device tensirs were previously stored and 
        load to correct current device (cpu or gpu)
        input: 1. previous_device file stores 1 line showing whereas file is GPU or CPU
        2. model stored in trained_state
        TODO: exception checks during import
        '''
        with open(os.path.join(path, 'previous_device'), 'w') as f:
            previous_device = f.read()'

        if previous_device not in ['cpu','cuda']:
            raise ValueError(f'Device {previous_device} is not supported. \
                Device should be either cpu or cuda')

        current_device = torch.device(self.device)
        if previous_device == 'cpu' and self.device == 'cuda':
            checkpoint = torch.load(os.path.join(path, fname)), map_location='cuda:0')
        elif previous_device == 'cuda' and self.device == 'cpu':   
            checkpoint=torch.load(os.path.join(path, fname)), map_location=current_device)

        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.optimizer.load_state_dict(checkpoint['optimizer_state_dict'])
        loss=checkpoint['loss']
        self.model.train()
        if self.device == 'cuda':
            self.model.to(current_device)

    def train(self, dataset:ImageLoader):

        running_loss = 0.0
        for batch in dataset:
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = batch
            outputs = self.model(inputs)
            loss = self.loss_function(outputs, labels)
            loss.backward()
            self.optimizer.step()

    def evaluate(self, dataset:ImageLoader):

        # set model to evaluation mode
        self.model.eval()

        # compute metrics over the dataset
        total=0
        correct=0
        for batch in dataset:
            # get the inputs; data is a list of [inputs, labels]
            inputs, labels = batch
            
            # compute model output
            outputs = model(inputs)

            # compute all metrics on this batch
            _, predicted = torch.max(outputs.data, 1)
            total += labels.size(0)
            correct += (predicted == labels).sum().item()

        # compute mean of all metrics in summary
        return correct / total
    
    def train_and_evaluate(self, train_dataloader, val_dataloader, num_epochs, restore_file):

        # reload weights from restore_file if specified
        if restore_file:
            self.logger.info(f"Restoring parameters from trained_state.pkl")
            self.load_checkpoint()

        for epoch in range(params.num_epochs):
            # Run one epoch
            self.logger.info(f"Starting epoch {epoch}")
            self.logger.info(f"Running training")
            self.train(train_dataloader)
            self.logger.info(f"Running validation")
            # Evaluate for one epoch on validation set
            val_metrics=self.evaluate(val_dataloader)
            self.logger.info(f'Accuracy of the network on the 10000 test images {val_metrics}')
            epoch_metrics=db.TrainingInfo(accuracy=val_metrics, epoch=epoch)
            self.db_session.add(epoch_metrics)