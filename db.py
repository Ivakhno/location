from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy import Column, Integer, String, Float
from sqlalchemy.ext.declarative import declarative_base

sql_config_uri = {
    'development': 'sqlite:////34.34.34.34:80/Development.db',
}

# configure Session class with desired options
Session = sessionmaker()
# later, we create the engine
engine = create_engine(sql_config_uri['development'], convert_unicode=True)
# associate it with our custom Session class
Session.configure(bind=engine)
Base = declarative_base()


class TrainingInfo(Base):
    __tablename__ = 'TrainingInfo'
    id = Column(Integer, primary_key=True)
    accuracy = Column(Float)
    epoch = Column(Integer)

    def __init__(self, accuracy, epoch):
        self.accuracy = accuracy
        self.epoch = epoch


def init_table():
    Base.metadata.create_all(engine)
    # work with the session
    return Session()
        