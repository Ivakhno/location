import random 
import os
from PIL import Image
import numpy as np
import logging
import logging.config
_path = os.path.dirname(os.path.abspath(__file__))

class ImageLoader(object):
    def __init__(self, directory, batch_size, device, cash_file):
        # setup logger 
        fileConfig(f'{_path}/serving.ini',
                disable_existing_loggers=False)
        self.logger = logging.getLogger(self.__class__.__name__)
        tmp_files = []
        cities = os.listdir()

        self.label_map = dict()
        i = 0
        for city in cities:
            self.label_map[city] = i
            i += 1
        if not cash_file:
            self.logger.info('reading all image files')
            for city in cities:
                path = os.path.join(directory, city)
                tmp_files.extend([os.join(path, f) for f in listdir(path)
                    if isfile(os.join(path, f))])
            # randomly shuffle the order         
            self.images = tmp_files[random.randrange(len(self.images))]
        else:
            self.logger.info('reading unseen image files from cash list')
            self.read_from_cash()
        self.batch_size = batch_size

    def write_to_cash(self):
        with open('cash.txt', 'w') as f:
            for item in self.images:
                f.writline(item)

    def read_from_cash(self):
        with open('cash.txt', 'w') as f:
            self.images = []
            for item in f:
                self.images.append(f.readline(item))

    @staticmethod
    def transform_image(self, image):
        '''
        grayscale and resize the data
        TODO: implement
        input: image of dimensions [height, width, channels]
        output: image of new dimensions [height, width, channels]
        '''

    @staticmethod
    def normalize_image(self, image):
        '''
        normalize the image pixels to 
        unit variance and zero mean
        TODO: implement
        '''

    @staticmethod
    def load_image(path):
        '''
        read image and transform 
        into tensor of dimensions 
        [batch_size, channels, height, width]
        '''
        img = Image.open(path)
        img.load()
        img = transform_image(img)
        img = normalize_image(img)
        np_array = np.asarray(img, dtype="int32")
        return torch.from_numpy(np_array).float().permute(2, 0, 1).to(device)        

    def __iter__(self):
        return self.images

    def __next__(self):
        '''
        return tensors of a given batch sizes
        create new cash of to-be-read files
        '''
        batch_x = []
        batch_y = []
        for path in self.images[0:self.batch_size]:
            y = self.label_map(path.split('/')[-2])
            batch_y.append(y)
            x = load_image(path)
            batch_x.append(x)
        del self.images[0:self.batch_size]
        self.write_to_cash()
        return torch.stack(batch_x), torch.tensor(batch_y)
