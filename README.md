# Image location recognition CNN classifier training

## Overview 

Image recognition classifier training uses locally mounted directory.
Users provide the path to training data images (as described in the challenge).
In addition, we assume that a separate directory with 'validation' images of the same format is provided.
The main 'Learner' class takes care of saving parameters after each Batch training in case the server shuts down.
The check is made to load model configuration to appropriate device (cpu or gpu if available).

Image reader class reads images and presents them as tensor batches using iterators.

Database uses ORM model.


## Additional Extensions
Currently network does not parallelize by GPU.  

## Installation


### Package

TODO: Use pipenv

### Docker

TODO: set up docker network via docker compose.

### Environment variables

TODO: To control access to resources (i.e. to make db connection secure), specify a separate .env configuration files.
These files can then be decrypted via `transcrypt` for example.  For more information see `https://github.com/elasticdog/transcrypt`.

## Usage

### Configuration

Currently parameters specifying versioning of docker images, models and s3 buckets are stored
in `src/config.py` (TODO: make as input argument to enable usage of multiple configs).  

### Training

Launch with default parameters using `pipenv run python ./main.py` or run
`./main.py --help`. Validation results for each epoch are saved to `TrainingInfo` SQL-Lite database, schema can be found under
`db.py`.

### Logging

Logging is available for both training and image processing functionality.

## Testing
TODO: add unit and integration tests with pytest

## Deployment 
TODO: add continious integration
