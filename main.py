import argparse
import os
from image_reader import ImageLoader
from network import Learner
import db
import torch

def run_training(conf):
    # create db sessions
    db_session = db.init_table()

    # fetch dataloaders
    training_data_loader = ImageLoader(conf.train_path, conf.batch, device, conf.resume)
    validation_data_loader = ImageLoader(conf.valid_path, conf.batch, device, conf.resume)

    # Define the model, optimizer and loss function
    lerner = Learner(db_session)

    # Train the model
    lerner.train_and_evaluate(training_data_loader, validation_data_loader, conf.epochs, conf.resume)


if __name__ =='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--epochs', type=int, default=10, help='training hyperparameter')
    parser.add_argument('--batch', type=int, default=64, help='training hyperparameter')
    parser.add_argument('--resume', action='store_true', help ='resume training flag')
    parser.add_argument('--train_path', help='path to locally mounted training data', type=str)
    parser.add_argument('--valid_path', help='path to locally mounted validation data', type=str)
    args, _ = parser.parse_known_args()
    run_training(args)
