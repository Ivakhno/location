from __future__ import absolute_import

import os
from glob import glob
import sys

from setuptools import setup, find_packages

# Declare minimal set for installation
required_packages = [
    "torch",
    "pil,
    "sqlalchemy"]

setup(
    name="City Location Recognition CNN trainer",
    version="0.1",
    packages=find_packages(),
    py_modules=[os.path.splitext(os.path.basename(path))[0] for path in glob("src/*.py")],
    install_requires=required_packages
)
